const path = require('path');

module.exports = {
  entry: './src_js/cmmn.js',
  output: {
    filename: 'cmmn.bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  performance: {
    maxEntrypointSize: 5242880,
    maxAssetSize: 5242880
  },
  module: {
    rules: [
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
};