import * as vscode from 'vscode';
import { Utils } from './Utils';

export class DMNEditorProvider implements vscode.CustomTextEditorProvider {
    private static readonly viewType = 'business-automation-tools.dmn';

    constructor(private readonly context: vscode.ExtensionContext) { }

    public static register(context: vscode.ExtensionContext): vscode.Disposable {
		const provider = new DMNEditorProvider(context);
		return vscode.window.registerCustomEditorProvider(DMNEditorProvider.viewType, provider);
	}

    resolveCustomTextEditor(document: vscode.TextDocument, webviewPanel: vscode.WebviewPanel, token: vscode.CancellationToken): void | Thenable<void> {
        webviewPanel.webview.options = {
            enableScripts: true,
        };

        webviewPanel.webview.html = this.getHtmlForWebview(webviewPanel.webview);
        
        webviewPanel.onDidChangeViewState(() => {
            if (webviewPanel.visible) {
                webviewPanel.webview.postMessage({
                    type: 'load-content',
                    data: document.getText()
                });
            }
        });

        webviewPanel.webview.onDidReceiveMessage(e => {
            switch(e.type) {
                case 'error':
                    vscode.window.showErrorMessage(e.data);
                    break;
                case 'get-content':
                    const content = e.data as string;
                    const edit = new vscode.WorkspaceEdit();
                    edit.replace(document.uri, new vscode.Range(0, 0, document.lineCount, 0), content);
                    vscode.workspace.applyEdit(edit);
                    break;
            }
		});
        
        setTimeout(() => {
            if (document.getText().length === 0) {
                webviewPanel.webview.postMessage({
                    type: 'load-content',
                    data: this.getInitialDiagram()
                });
            } else {
                webviewPanel.webview.postMessage({
                    type: 'load-content',
                    data: document.getText()
                });
            }
        }, 1000);
    }

    private getHtmlForWebview(webview: vscode.Webview): string {
        const dmnJSUri = webview.asWebviewUri(vscode.Uri.joinPath(this.context.extensionUri, 'dist', 'dmn.bundle.js'));

        const nonce = Utils.getNonce();

        const configuration = vscode.workspace.getConfiguration('businessAutomationTools');

        const dmnConfiguration = JSON.stringify(configuration.get("dmn"));

        return `
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="Content-Security-Policy" content="script-src 'nonce-${nonce}';">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>DMN</title>
                <style>
                    html, body, #canvas {
                        height: 100%;
                        padding: 0;
                        margin: 0;
                        background-color: white;
                        color: #444;
                    }

                    #canvas {
                        width: 80%;
                        float: left;
                    }

                    #properties {
                        width: 20%;
                        float: right;
                        height: 100%;
                    }

                    .dpp-properties-panel {
                        height: 100%;
                    }

                    .dpp-properties-panel [type=text] {
                        width: 85%
                    }

                    .dpp-properties-panel [contenteditable] {
                        width: 95%
                    }

                    .dpp-textfield input {
                        padding-right: 20px;
                    }
                </style>
            </head>
            <body>
                <div id="canvas"></div>
                <div id="properties"></div>
                <script nonce="${nonce}">
                    const dmnConfiguration = JSON.parse('${dmnConfiguration}');
                </script>
                <script nonce="${nonce}" src="${dmnJSUri}"></script>
            </body>
        </html>`;
    }

    private getInitialDiagram() {
        return `
        <?xml version="1.0" encoding="UTF-8"?>
        <definitions 
            xmlns="https://www.omg.org/spec/DMN/20191111/MODEL/"
            namespace="http://camunda.org/schema/1.0/dmn"
            id="definitions_1"
            name="definitions">
          <decision id="decision_1" name="">
            <decisionTable id="decisionTable_1">
              <input id="input1" label="">
                <inputExpression id="inputExpression1" typeRef="string">
                  <text></text>
                </inputExpression>
              </input>
              <output id="output1" label="" name="" typeRef="string" />
            </decisionTable>
          </decision>
        </definitions>
        `;
    }
}