import * as vscode from 'vscode';
import { Utils } from './Utils';

export class BPMNEditorProvider implements vscode.CustomTextEditorProvider {
    private static readonly viewType = 'business-automation-tools.bpmn';

    constructor(private readonly context: vscode.ExtensionContext) { }

    public static register(context: vscode.ExtensionContext): vscode.Disposable {
		const provider = new BPMNEditorProvider(context);
		return vscode.window.registerCustomEditorProvider(BPMNEditorProvider.viewType, provider);
	}

    resolveCustomTextEditor(document: vscode.TextDocument, webviewPanel: vscode.WebviewPanel, token: vscode.CancellationToken): void | Thenable<void> {
        webviewPanel.webview.options = {
            enableScripts: true,
        };

        webviewPanel.webview.html = this.getHtmlForWebview(webviewPanel.webview);
        
        webviewPanel.onDidChangeViewState(() => {
            if (webviewPanel.visible) {
                webviewPanel.webview.postMessage({
                    type: 'load-content',
                    data: document.getText()
                });
            }
        });

        webviewPanel.webview.onDidReceiveMessage(e => {
            switch(e.type) {
                case 'error':
                    vscode.window.showErrorMessage(e.data);
                    break;
                case 'get-content':
                    const content = e.data as string;                    
                    const edit = new vscode.WorkspaceEdit();
                    edit.replace(document.uri, new vscode.Range(0, 0, document.lineCount, 0), content);
                    vscode.workspace.applyEdit(edit);
                    break;
            }
		});

        setTimeout(() => {
            if (document.getText().length === 0) {
                webviewPanel.webview.postMessage({
                    type: 'create-default-content'
                });
            } else {
                webviewPanel.webview.postMessage({
                    type: 'load-content',
                    data: document.getText()
                });
            }
        }, 1000);
    }

    private getHtmlForWebview(webview: vscode.Webview): string {
        const bpmnJSUri = webview.asWebviewUri(vscode.Uri.joinPath(this.context.extensionUri, 'dist', 'bpmn.bundle.js'));

        const nonce = Utils.getNonce();

        const configuration = vscode.workspace.getConfiguration('businessAutomationTools');

        const bpmnConfiguration = JSON.stringify(configuration.get("bpmn"));

        return `
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="Content-Security-Policy" content="script-src 'nonce-${nonce}';">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>BPMN</title>
                <style>
                    html, body, #canvas {
                        height: 100%;
                        padding: 0;
                        margin: 0;
                        background-color: white;
                        color: #444;
                    }

                    #canvas {
                        width: 80%;
                        float: left;
                    }

                    #properties {
                        width: 20%;
                        float: right;
                        height: 100%;
                    }

                    .bpp-properties-panel {
                        height: 100%;
                    }

                    .bpp-properties-panel [type=text] {
                        width: 85%
                    }

                    .bpp-properties-panel [contenteditable] {
                        width: 95%
                    }

                    .bpp-textfield input {
                        padding-right: 20px;
                    }
                </style>
            </head>
            <body>
                <div id="canvas"></div>
                <div id="properties"></div>
                <script nonce="${nonce}">
                    const bpmnConfiguration = JSON.parse('${bpmnConfiguration}');
                </script>
                <script nonce="${nonce}" src="${bpmnJSUri}"></script>
            </body>
        </html>`;
    }
}