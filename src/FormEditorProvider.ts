import * as vscode from 'vscode';
import { Utils } from './Utils';

export class FormEditorProvider implements vscode.CustomTextEditorProvider {
    private static readonly viewType = 'business-automation-tools.form';

    constructor(private readonly context: vscode.ExtensionContext) { }

    public static register(context: vscode.ExtensionContext): vscode.Disposable {
        const provider = new FormEditorProvider(context);
        return vscode.window.registerCustomEditorProvider(FormEditorProvider.viewType, provider);
    }

    resolveCustomTextEditor(document: vscode.TextDocument, webviewPanel: vscode.WebviewPanel, token: vscode.CancellationToken): void | Thenable<void> {
        webviewPanel.webview.options = {
            enableScripts: true,
        };

        webviewPanel.webview.html = this.getHtmlForWebview(webviewPanel.webview);

        webviewPanel.onDidChangeViewState(() => {
            if (webviewPanel.visible) {
                webviewPanel.webview.postMessage({
                    type: 'load-content',
                    data: document.getText()
                });
            }
        });

        webviewPanel.webview.onDidReceiveMessage(e => {
            switch (e.type) {
                case 'error':
                    vscode.window.showErrorMessage(e.data);
                    break;
                case 'get-content':
                    const content = e.data as string;
                    const edit = new vscode.WorkspaceEdit();
                    edit.replace(document.uri, new vscode.Range(0, 0, document.lineCount, 0), content);
                    vscode.workspace.applyEdit(edit);
                    break;
            }
        });

        setTimeout(() => {
            if (document.getText().length === 0) {
                webviewPanel.webview.postMessage({
                    type: 'create-default-content'
                });
            } else {
                webviewPanel.webview.postMessage({
                    type: 'load-content',
                    data: document.getText()
                });
            }
        }, 1000);
    }

    private getHtmlForWebview(webview: vscode.Webview): string {
        const formJSUri = webview.asWebviewUri(vscode.Uri.joinPath(this.context.extensionUri, 'dist', 'form.bundle.js'));

        const nonce = Utils.getNonce();

        return `
        <!DOCTYPE html>
        <html lang="en">
            <head>
                <meta charset="UTF-8">
                <meta http-equiv="Content-Security-Policy" content="script-src 'nonce-${nonce}';">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>FORM</title>
                <style>
                    html, body {
                        height: 100%;
                        padding: 0;
                        margin: 0;
                        background-color: white;
                        color: #444;
                    }

                    #form {
                        max-width: 100%;
                    }
                </style>
            </head>
            <body>
                <div id="form"></div>
                <script nonce="${nonce}" src="${formJSUri}"></script>
            </body>
        </html>`;
    }
}
