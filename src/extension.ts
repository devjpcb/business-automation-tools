// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import { BPMNEditorProvider } from './BPMNEditorProvider';
import { CMMNEditorProvider } from './CMMNEditorProvider';
import { DMNEditorProvider } from './DMNEditorProvider';
import { FormEditorProvider } from './FormEditorProvider';

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(BPMNEditorProvider.register(context));
	context.subscriptions.push(CMMNEditorProvider.register(context));
	context.subscriptions.push(DMNEditorProvider.register(context));
	context.subscriptions.push(FormEditorProvider.register(context));
}

// this method is called when your extension is deactivated
export function deactivate() { }
