# Business Automation Tools

Create and edit BPMN, DMN, CMMN diagrams in VS Code using [bpmn.io](https://bpmn.io) tools.

## Features

* Create and edit BPMN
* Create and edit DMN
* Create and edit CMMN
* Create and edit FORM

![feature 1](images/feature-1.png)

![feature 2](images/feature-2.png)

![feature 3](images/feature-3.png)

![feature 4](images/feature-4.png)

## Requirements

N/A

## Extension Files
    .bpmn
	.bpmn2
	.bpmn20.xml
	.dmn
	.cmmn
    .form
	.form.json

## Extension Settings
* **businessAutomationTools.bpmn.enablePropertiesPanel:** Enable / Disable BPMN Properties Panel

* **businessAutomationTools.bpmn.enableColorPicker:** Enable / Disable BPMN Color Picker

* **businessAutomationTools.bpmn.enableTokenSimulator:** Enable / Disable BPMN Token Simulator

* **businessAutomationTools.bpmn.extensionProperties:** Extension Properties

* **businessAutomationTools.dmn.enablePropertiesPanel:** Enable / Disable DMN Properties Panel

* **businessAutomationTools.cmmn.enablePropertiesPanel:** Enable / Disable CMMN Properties Panel

![feature 2](images/settings.png)

## Known Issues

N/A

## Release Notes

## [1.5.7] - 2025-02-19
### Added
- Updates to the latest version of bpmn.io

## [1.5.6] - 2024-11-27
### Added
- Updates to the latest version of bpmn.io

## [1.5.5] - 2024-07-07
### Added
- Updates to the latest version of bpmn.io

## [1.5.4] - 2024-07-07
### Added
- Updates to the latest version of bpmn.io

## [1.5.3] - 2024-05-05
### Added
- Updates to the latest version of bpmn.io

## [1.5.2] - 2024-04-14
### Added
- Updates to the latest version of bpmn.io

## [1.5.1] - 2024-03-31
### Added
- Updates to the latest version of bpmn.io

## [1.5.0] - 2024-03-16
### Added
- Create and edit FORM

## [1.4.4] - 2024-03-02
### Added
- Updates to the latest version of bpmn.io

## [1.4.3] - 2024-02-25
### Added
- Added support for Camunda properties in BPMN
- Added support for Token Simulator
- All modules are now disabled by defects

## [1.4.2] - 2024-02-20
### Added
- Added settings to turn the properties panel enable or disable
- Now when you open a dmn it is automatically migrated to version 1.3

## [1.4.1] - 2024-02-13
### Added
- CMMN: Properties panel is added

## [1.4.0] - 2024-02-13
### Added
- Updates to the latest version of bpmn.io
- The project is refactored

## [1.3.0] - 2021-12-11
### Added
- DMN: Properties panel is added

## [1.2.0] - 2021-12-11
### Added
- BPMN: Properties panel and color selector are added

## [1.1.0] - 2021-12-08
### Added
- Create and edit CMMN

## [1.0.0] - 2021-12-04
### Added
- Create and edit BPMN
- Create and edit DMN
