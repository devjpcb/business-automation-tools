# Change Log

All notable changes to the "business-automation-tools" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [1.5.7] - 2025-02-19
### Added
- Updates to the latest version of bpmn.io

## [1.5.6] - 2024-11-27
### Added
- Updates to the latest version of bpmn.io

## [1.5.5] - 2024-07-07
### Added
- Updates to the latest version of bpmn.io

## [1.5.4] - 2024-07-07
### Added
- Updates to the latest version of bpmn.io

## [1.5.3] - 2024-05-05
### Added
- Updates to the latest version of bpmn.io

## [1.5.2] - 2024-04-14
### Added
- Updates to the latest version of bpmn.io

## [1.5.1] - 2024-03-31
### Added
- Updates to the latest version of bpmn.io

## [1.5.0] - 2024-03-16
### Added
- Create and edit FORM

## [1.4.4] - 2024-03-02
### Added
- Updates to the latest version of bpmn.io

## [1.4.3] - 2024-02-25
### Added
- Added support for Camunda properties in BPMN
- Added support for Token Simulator
- All modules are now disabled by defects

## [1.4.2] - 2024-02-20
### Added
- Added settings to turn the properties panel enable or disable
- Now when you open a dmn it is automatically migrated to version 1.3

## [1.4.1] - 2024-02-13
### Added
- CMMN: Properties panel is added

## [1.4.0] - 2024-02-13
### Added
- Updates to the latest version of bpmn.io
- The project is refactored

## [1.3.0] - 2021-12-11
### Added
- DMN: Properties panel is added

## [1.2.0] - 2021-12-11
### Added
- BPMN: Properties panel and color selector are added

## [1.1.0] - 2021-12-08
### Added
- Create and edit CMMN

## [1.0.0] - 2021-12-04
### Added
- Create and edit BPMN
- Create and edit DMN