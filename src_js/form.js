import '@bpmn-io/form-js/dist/assets/form-js.css';
import '@bpmn-io/form-js/dist/assets/form-js-editor.css';

import { FormEditor } from '@bpmn-io/form-js';

const vscode = acquireVsCodeApi();

const formEditor = new FormEditor({
    container: document.querySelector('#form')
});

const defaultSchema = {
    "type": "default"
};

formEditor.on('commandStack.changed', function () {
    var dataSchema = formEditor.saveSchema();

    vscode.postMessage({
        type: 'get-content',
        data: JSON.stringify(dataSchema, null, 4)
    });
});

window.addEventListener('message', event => {
    const message = event.data;

    switch (message.type) {
        case 'create-default-content':
            formEditor.importSchema(defaultSchema).catch(err => {
                vscode.postMessage({
                    type: 'error',
                    data: err
                });
            });
            break;
        case 'load-content':
            var dataSchema = JSON.parse(message.data);

            formEditor.importSchema(dataSchema).catch(err => {
                vscode.postMessage({
                    type: 'error',
                    data: err
                });
            });
            break;
    }
});
