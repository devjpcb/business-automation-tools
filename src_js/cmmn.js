import 'cmmn-js/dist/assets/diagram-js.css';
import 'cmmn-js/dist/assets/cmmn-font/css/cmmn.css';
import 'cmmn-js-properties-panel/dist/assets/cmmn-js-properties-panel.css';

import CmmnModeler from 'cmmn-js/lib/Modeler';

import CmmnPropertiesPanelModule from 'cmmn-js-properties-panel';
import CmmnPropertiesProviderModule from 'cmmn-js-properties-panel/lib/provider/cmmn';

const vscode = acquireVsCodeApi();

let cmmnModules = [];

if (cmmnConfiguration.enablePropertiesPanel) {
    cmmnModules.push(CmmnPropertiesPanelModule);
    cmmnModules.push(CmmnPropertiesProviderModule);
    document.getElementById('canvas').style.width = '80%';
    document.getElementById('properties').style.width = '20%';
} else {
    document.getElementById('canvas').style.width = '100%';
    document.getElementById('properties').style.width = '0%';
}

const cmmnModeler = new CmmnModeler({
    container: '#canvas',
    propertiesPanel: {
        parent: '#properties'
    },
    additionalModules: cmmnModules,
    keyboard: { bindTo: document }
});

cmmnModeler.on('commandStack.changed', function () {
    cmmnModeler.saveXML({ format: true }, function (err, xml) {
        if (!err) {
            vscode.postMessage({
                type: 'get-content',
                data: xml
            });
        }
    });
});

window.addEventListener('message', event => {
    const message = event.data;

    switch (message.type) {
        case 'create-default-content':
            cmmnModeler.createDiagram();
            break;
        case 'load-content':
            cmmnModeler.importXML(message.data, function (err) {
                if (err) {
                    vscode.postMessage({
                        type: 'error',
                        data: err
                    });
                }
            });
            break;
    }
});