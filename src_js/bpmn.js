import 'bpmn-js/dist/assets/diagram-js.css';
import 'bpmn-js/dist/assets/bpmn-js.css';
import 'bpmn-js/dist/assets/bpmn-font/css/bpmn-embedded.css';
import 'bpmn-js-token-simulation/assets/css/bpmn-js-token-simulation.css';
import '@bpmn-io/properties-panel/dist/assets/properties-panel.css';
import 'bpmn-js-color-picker/colors/color-picker.css';

import BpmnModeler from 'bpmn-js/lib/Modeler';

import {
    BpmnPropertiesPanelModule,
    BpmnPropertiesProviderModule,
    ZeebePropertiesProviderModule,
    CamundaPlatformPropertiesProviderModule
} from 'bpmn-js-properties-panel';

import BpmnColorPickerModule from 'bpmn-js-color-picker';
import ZeebeBpmnModdle from 'zeebe-bpmn-moddle/resources/zeebe.json';
import CamundaBpmnModdle from 'camunda-bpmn-moddle/resources/camunda.json';
import TokenSimulationModule from 'bpmn-js-token-simulation';

const vscode = acquireVsCodeApi();

let bpmnModules = [];
let bpmnExtensions = {};

if (bpmnConfiguration.enablePropertiesPanel) {
    bpmnModules.push(BpmnPropertiesPanelModule);
    bpmnModules.push(BpmnPropertiesProviderModule);
    document.getElementById('canvas').style.width = '80%';
    document.getElementById('properties').style.width = '20%';
} else {
    document.getElementById('canvas').style.width = '100%';
    document.getElementById('properties').style.width = '0%';
}

if (bpmnConfiguration.enableColorPicker) {
    bpmnModules.push(BpmnColorPickerModule);
}

if (bpmnConfiguration.enableTokenSimulator) {
    bpmnModules.push(TokenSimulationModule);
}

if (bpmnConfiguration.extensionProperties === 'cloud') {
    bpmnModules.push(ZeebePropertiesProviderModule);
    bpmnExtensions = {
        zeebe: ZeebeBpmnModdle
    };
} else if (bpmnConfiguration.extensionProperties === 'platform') {
    bpmnModules.push(CamundaPlatformPropertiesProviderModule);
    bpmnExtensions = {
        camunda: CamundaBpmnModdle
    };
}

const bpmnModeler = new BpmnModeler({
    container: '#canvas',
    propertiesPanel: {
        parent: '#properties'
    },
    additionalModules: bpmnModules,
    moddleExtensions: bpmnExtensions,
    keyboard: { bindTo: document }
});

bpmnModeler.on('commandStack.changed', function () {
    bpmnModeler.saveXML({ format: true }).then(obj => {
        vscode.postMessage({
            type: 'get-content',
            data: obj.xml
        });
    }).catch(err => {
        vscode.postMessage({
            type: 'error',
            data: err
        });
    });
});

window.addEventListener('message', event => {
    const message = event.data;

    switch (message.type) {
        case 'create-default-content':
            bpmnModeler.createDiagram();
            break;
        case 'load-content':
            bpmnModeler.importXML(message.data).catch(err => {
                vscode.postMessage({
                    type: 'error',
                    data: err
                });
            });
            break;
    }
});