import 'dmn-js/dist/assets/diagram-js.css';
import 'dmn-js/dist/assets/dmn-js-shared.css';
import 'dmn-js/dist/assets/dmn-js-drd.css';
import 'dmn-js/dist/assets/dmn-js-decision-table.css';
import 'dmn-js/dist/assets/dmn-js-decision-table-controls.css';
import 'dmn-js/dist/assets/dmn-js-literal-expression.css';
import 'dmn-js/dist/assets/dmn-font/css/dmn.css';
import 'dmn-js-properties-panel/dist/assets/properties-panel.css';

import DmnModeler from 'dmn-js/lib/Modeler';
import { DmnPropertiesPanelModule, DmnPropertiesProviderModule } from 'dmn-js-properties-panel';
import { migrateDiagram } from '@bpmn-io/dmn-migrate';

const vscode = acquireVsCodeApi();

let dmnModules = [];

if (dmnConfiguration.enablePropertiesPanel) {
    dmnModules.push(DmnPropertiesPanelModule);
    dmnModules.push(DmnPropertiesProviderModule);
    document.getElementById('canvas').style.width = '80%';
    document.getElementById('properties').style.width = '20%';
} else {
    document.getElementById('canvas').style.width = '100%';
    document.getElementById('properties').style.width = '0%';
}

const dmnModeler = new DmnModeler({
    drd: {
        propertiesPanel: {
            parent: '#properties'
        },
        additionalModules: dmnModules
    },
    container: '#canvas',
    keyboard: { bindTo: document }
});

window.addEventListener('message', async event => {
    const message = event.data;

    switch (message.type) {
        case 'load-content':
            let data = await migrateDiagram(message.data);
            dmnModeler.importXML(data).then(obj => {
                const listTypes = new Set();

                dmnModeler.getViews().forEach(function (view) {
                    const viewer = dmnModeler._getViewer(view);
                    const eventBus = viewer.get('eventBus');

                    if (!listTypes.has(view.type)) {
                        listTypes.add(view.type);
                        eventBus.on('commandStack.changed', function (event) {
                            dmnModeler.saveXML({ format: true }).then(obj => {
                                vscode.postMessage({
                                    type: 'get-content',
                                    data: obj.xml
                                });
                            });
                        });
                    }
                });
            }).catch(err => {
                vscode.postMessage({
                    type: 'error',
                    data: err
                });
            });
            break;
    }
});